#!/usr/bin/env bash

test -n "$BOT_TOKEN" || export BOT_TOKEN="$1"

if [ -z "$BOT_TOKEN" ]
then
    echo
    echo -en "\e[31m"
    echo -n Bot token must be provided in BOT_TOKEN environment variable or passed as script argument!
    echo -e "\e[0m"
    echo
    echo "Usage: ./run.sh <bot_token>"
    echo
    exit
fi >&2

which pipenv || pip3 install pipenv
pipenv install

pipenv run python src/main.py