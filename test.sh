#!/usr/bin/env bash

TESTDIR=_testdir

export BOT_TOKEN=some_secret_token
export HOME_CHAT=100500

rm -rf $TESTDIR
mkdir $TESTDIR

cp -r tests/test-data $TESTDIR/data
cp -r src $TESTDIR/src
cp -r tests/*.py $TESTDIR/src

if [ "$1" == '--no-test' ]
then
    exit
elif [ -n "$1" ]
then
    echo -en "\e[31m"
    echo -n Unrecognized options
    echo -e "\e[0m"
    exit
fi

cd $TESTDIR
pipenv run pytest -vv src