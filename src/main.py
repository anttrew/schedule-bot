"""Main module that runs bot itself."""

import logging
from asyncio import create_task

from aiogram import Bot, Dispatcher, executor
from aiogram.types import ParseMode

from commands import register_commands
from helpers.env import BOT_TOKEN
from notifier import run_notify_loop


async def on_startup(_: Dispatcher):
    """Run notifier in async loop."""
    create_task(run_notify_loop(bot))


if __name__ == "__main__":
    bot = Bot(token=BOT_TOKEN, parse_mode=ParseMode.HTML)

    dp = Dispatcher(bot)
    register_commands(dp)

    logging.basicConfig(level=logging.INFO)

    executor.start_polling(dp, skip_updates=True, on_startup=on_startup)
