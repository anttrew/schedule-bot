"""Useful wrappers for Telegram message sending API."""

from typing import Any

from aiogram import Bot
from aiogram.types import Message

DEFAULT_MESSAGE_PARAMS: dict[str, Any] = {"disable_web_page_preview": True}


async def reply(msg: Message, text: str):
    """Reply on given message with given text."""
    await msg.reply(text, **DEFAULT_MESSAGE_PARAMS)


async def send_message(bot: Bot, chat_id: int, text: str):
    """Send given text to given chat."""
    await bot.send_message(chat_id, text, **DEFAULT_MESSAGE_PARAMS)
