"""Function for retreiving information about current and next lesson."""

from datetime import date, datetime, time
from typing import Optional

from helpers.data import get_lessons_time, get_schedule


def get_current_lesson() -> Optional[int]:
    """Get index of current lesson."""
    now: time = datetime.now().time()
    schedule: list[str] = get_schedule()[date.today().weekday()]
    for i, (start_time,
            end_time) in enumerate(get_lessons_time()[:len(schedule)]):
        if start_time <= now < end_time and schedule[i]:
            return i
    return None


def get_next_lesson() -> Optional[int]:
    """Get index of next lesson."""
    now: time = datetime.now().time()
    today_schedule: list[str] = get_schedule()[date.today().weekday()]
    for i, (start, _) in enumerate(get_lessons_time()[:len(today_schedule)]):
        if now < start and today_schedule[i]:
            return i

    return None
