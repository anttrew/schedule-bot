"""Data loaders.

Functions for loading schedule data from JSON.
"""

import json
from datetime import time
from typing import Any


def load_data() -> dict[str, Any]:
    """Load data from JSON file."""
    return json.load(open('data/lessons.json', encoding='utf-8'))


def get_lessons_time() -> list[tuple[time, time]]:
    """Get list of lesson start and end times."""
    return [(time.fromisoformat(start), time.fromisoformat(end))
            for start, end in load_data()['time']]


def get_lessons_info() -> dict[str, str]:
    """Get lessons information."""
    return load_data()['info']


def get_schedule() -> list[list[str]]:
    """Get list of lessons on weekdays."""
    return load_data()['schedule']


def is_online(subject: str) -> bool:
    """Get whether the subject's lessons are online."""
    return load_data()['online'][subject]
