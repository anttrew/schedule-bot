"""Bot command handlers."""

from datetime import date, timedelta
from typing import Callable, Coroutine, Optional

from aiogram import Dispatcher
from aiogram.types import Message

from helpers.format import (bold, get_lesson_representation,
                            get_schedule_representation)
from helpers.lessons import get_current_lesson, get_next_lesson
from helpers.messages import reply

HELP_STRING: str = """
/schedule - расписание на сегодня
/schedule &lt;день недели&gt; - расписание на день недели
/tomorrow - расписание на завтра
/now - текущий урок
/next - следующий урок
/help - помощь
"""


def command_help(_args: list[str]) -> str:
    """Handle /help command."""
    return HELP_STRING


def command_schedule(args: list[str]) -> str:
    """Handle /schedule command."""
    weekdays: list[str] = [
        'пн',
        'вт',
        'ср',
        'чт',
        'пт',
        'сб',
        'вс'
    ]

    try:
        weekday: int = weekdays.index(args[0].lower())
    except IndexError:
        weekday: int = date.today().weekday()
    except ValueError:
        return bold("Неправильный формат дня недели")

    return get_schedule_representation(weekday)


def command_tomorrow(_args: list[str]) -> str:
    """Handle /tomorrow command."""
    return get_schedule_representation(
        (date.today() + timedelta(days=1)).weekday()
    )


def command_now(_args: list[str]) -> str:
    """Handle /now command."""
    weekday: int = date.today().weekday()
    lesson: Optional[int] = get_current_lesson()

    if lesson is None:
        return command_next([])
    return f'{bold("Сейчас идёт:")}\n\n' \
        + get_lesson_representation(weekday, lesson)


def command_next(_args: list[str]) -> str:
    """Handle /next command."""
    weekday: int = date.today().weekday()
    lesson: Optional[int] = get_next_lesson()

    if lesson is None:
        return bold("Сегодня больше нет уроков")
    return f'{bold("Следующий урок:")}\n\n' + \
        get_lesson_representation(weekday, lesson)


def wrap(command: Callable) -> Callable[[Message], Coroutine]:
    """Wrap raw-text command function into asynchronous command handler."""
    async def wrapper(message: Message):
        res = command(message.text.split()[1:])
        await reply(message, res)

    return wrapper


def register_commands(dispatch: Dispatcher):
    """Register bot commands."""
    for (name, command) in commands.items():
        dispatch.register_message_handler(
            wrap(command), commands=[name])


commands = {
    'help': command_help,
    'schedule': command_schedule,
    'tomorrow': command_tomorrow,
    'now': command_now,
    'next': command_next
}
