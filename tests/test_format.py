"""Test text formatting."""

from datetime import date, datetime, timedelta

from freezegun import freeze_time

from helpers.format import (bold, get_lesson_line, get_lesson_representation,
                            get_schedule_representation, get_time_comment)


def test_bold():
    """Test `helpers.format.bold` function."""
    assert bold("SOME STRANGE TEXT") == "<b>SOME STRANGE TEXT</b>"
    assert bold("TEST\nNEWLINE") == "<b>TEST\nNEWLINE</b>"
    assert bold("") == ""


def test_time_comment():
    """Test `helpers.format.get_time_comment` function."""
    assert get_time_comment([1, 2, 3, 4, 5], datetime(1, 1, 1, 8, 35),
                            datetime(1, 1, 1, 9, 0)) \
        is None
    assert get_time_comment([21], datetime(1, 1, 1, 13, 0, 1),
                            datetime(1, 1, 1, 13, 22)) \
        == "Через 21 минуту начнётся"
    assert get_time_comment([0, 3], datetime(1, 1, 1, 18, 47),
                            datetime(1, 1, 1, 18, 50)) \
        == "Через 3 минуты начнётся"
    assert get_time_comment([65], datetime(1, 1, 1, 7, 50),
                            datetime(1, 1, 1, 8, 55)) \
        == "Через 65 минут начнётся"
    assert get_time_comment([0], datetime(1, 1, 1, 0, 1, 1),
                            datetime(1, 1, 1, 0, 1)) \
        is None
    assert get_time_comment([0], datetime(1, 1, 1, 12, 30),
                            datetime(1, 1, 1, 12, 30)) \
        == "Сейчас начнётся"
    assert get_time_comment([0, 100], datetime(1, 1, 1, 0, 59, 1),
                            datetime(1, 1, 1, 1, 0)) \
        == "Сейчас начнётся"


def test_lesson_line():
    """Test `helpers.format.get_lesson_line` function."""
    assert get_lesson_line(3, 5) == "Физика (18:00 - 19:00)"
    assert get_lesson_line(5, 2) == ""
    assert get_lesson_line(0, 0) == "Алгебра (08:00 - 09:00)"


def test_lesson_representation():
    """Test `helpers.format.get_lesson_representation` function."""
    assert get_lesson_representation(5, 2) == "\n\n"
    assert get_lesson_representation(3, 1) \
        == "Всеобщая история (10:00 - 11:00)\n\nСсылочка на телегу Полины"
    assert get_lesson_representation(3, 3) \
        == "Биология (14:00 - 15:00)\n\nЗаразная инфа"


def test_schedule_representation():
    """Test `helpers.format.get_schedule_representation` function."""
    correct_schedules = [
        "1. Алгебра (08:00 - 09:00)\n"
        "2. Алгебра (10:00 - 11:00)\n"
        "3. Алгебра (12:00 - 13:00)\n"
        "4. Английский язык (14:00 - 15:00)\n"
        "5. Английский язык (16:00 - 17:00)\n"
        "6. Английский язык (18:00 - 19:00)\n"
        "7. Обществознание (20:00 - 21:00)\n"
        "8. Обществознание (22:00 - 23:00)", "1. Литература (08:00 - 09:00)\n"
        "2. Литература (10:00 - 11:00)\n"
        "3. Физика (12:00 - 13:00)\n"
        "4. Физика (14:00 - 15:00)\n"
        "5. Физический практикум (16:00 - 17:00)\n"
        "6. Физический практикум (18:00 - 19:00)",
        "1. Математический практикум (08:00 - 09:00)\n"
        "2. Математический практикум (10:00 - 11:00)\n"
        "3. Геометрия (12:00 - 13:00)\n"
        "4. Геометрия (14:00 - 15:00)\n"
        "5. Информатика (16:00 - 17:00)\n"
        "6. Информатика (18:00 - 19:00)",
        "1. Всеобщая история (08:00 - 09:00)\n"
        "2. Всеобщая история (10:00 - 11:00)\n"
        "3. Биология (12:00 - 13:00)\n"
        "4. Биология (14:00 - 15:00)\n"
        "5. Физика (16:00 - 17:00)\n"
        "6. Физика (18:00 - 19:00)", "1. История России (08:00 - 09:00)\n"
        "2. История России (10:00 - 11:00)\n"
        "3. Математический анализ (12:00 - 13:00)\n"
        "4. Математический анализ (14:00 - 15:00)\n"
        "5. Английский язык (16:00 - 17:00)\n"
        "6. Английский язык (18:00 - 19:00)",
        "1. Античная история (08:00 - 09:00)\n"
        "2. Античная история (10:00 - 11:00)\n"
        "3. \n"
        "4. \n"
        "5. Химия (16:00 - 17:00)\n"
        "6. ОБЖ (18:00 - 19:00)", "<b>Завтра нет уроков</b>"
    ]

    real_today: datetime = datetime.today()
    for i in range(7):
        day: date = (real_today + timedelta(days=i + 1)).date()
        with freeze_time(datetime.today() + timedelta(days=i)):
            assert get_schedule_representation(day.weekday()) \
                == correct_schedules[day.weekday()]

    with freeze_time("2021-11-11 12:30"):
        assert get_schedule_representation(datetime.now().date().weekday()) \
            == "1. Всеобщая история (08:00 - 09:00)\n"\
               "2. Всеобщая история (10:00 - 11:00)\n"\
               "<b>3. Биология (12:00 - 13:00) — сейчас</b>\n"\
               "4. Биология (14:00 - 15:00)\n"\
               "5. Физика (16:00 - 17:00)\n"\
               "6. Физика (18:00 - 19:00)"
