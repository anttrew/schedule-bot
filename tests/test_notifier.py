"""Test lesson notifier."""

from unittest.mock import AsyncMock

import pytest
from freezegun import freeze_time

from notifier import notify_lesson


@pytest.mark.asyncio
async def test_notify_lesson():
    """Test `notifier.notify_lesson` function."""
    with freeze_time("10 Nov 2021, 11:57:34"):
        bot = AsyncMock()
        await notify_lesson(bot, [2])
        bot.send_message.assert_called_with(100500,
                                            "<b>Через 2 минуты начнётся:</b>\n"
                                            "\n"
                                            "Геометрия (12:00 - 13:00)\n"
                                            "\n"
                                            "Аффинная инфа",
                                            disable_web_page_preview=True)

    with freeze_time("10 Nov 2021, 11:59:01"):
        bot = AsyncMock()
        await notify_lesson(bot, [0, 1, 2, 3, 4])
        bot.send_message.assert_called_with(100500, "<b>Сейчас начнётся:</b>\n"
                                            "\n"
                                            "Геометрия (12:00 - 13:00)\n"
                                            "\n"
                                            "Аффинная инфа",
                                            disable_web_page_preview=True)

    with freeze_time("24 Dec 2021, 15:59:01"):
        bot = AsyncMock()
        await notify_lesson(bot, [0, 1, 2, 3, 4])
        bot.send_message.assert_not_called()

    with freeze_time("10 Nov 2021, 12:00:01"):
        bot = AsyncMock()
        await notify_lesson(bot, [0, 1, 2, 3, 4])
        bot.send_message.assert_not_called()

    with freeze_time("10 Nov 2021, 11:50:00"):
        bot = AsyncMock()
        await notify_lesson(bot, [0, 1, 2, 3, 4])
        bot.send_message.assert_not_called()

    with freeze_time("10 Nov 2021, 11:59:31"):
        bot = AsyncMock()
        await notify_lesson(bot, [1, 2, 3, 4])
        bot.send_message.assert_not_called()

    with freeze_time("13 Nov 2021, 11:54:43"):
        bot = AsyncMock()
        await notify_lesson(bot, [0, 1, 2, 3, 4, 5, 6])
        bot.send_message.assert_not_called()
