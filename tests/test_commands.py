"""Test bot commands."""

from freezegun import freeze_time

from commands import command_next, command_now, command_schedule
from helpers.format import get_schedule_representation


def test_next():
    """Test /next command."""
    with freeze_time("13 Nov 2021, 08:00"):
        assert command_next([]) == \
            "<b>Следующий урок:</b>\n"\
            "\n"\
            "Античная история (10:00 - 11:00)\n"\
            "\n"\
            "Афинная инфа"
    with freeze_time("10 Nov 2021, 15:17"):
        assert command_next([]) == \
            "<b>Следующий урок:</b>\n"\
            "\n"\
            "Информатика (16:00 - 17:00)\n"\
            "\n"\
            "Дезинфа"
    with freeze_time("6 Nov 2021, 10:57"):
        assert command_next([]) == \
            "<b>Следующий урок:</b>\n"\
            "\n"\
            "Химия (16:00 - 17:00)\n"\
            "\n"\
            "Ядовитая инфа"
    with freeze_time("6 Nov 2021, 12:57"):
        assert command_next([]) == \
            "<b>Следующий урок:</b>\n"\
            "\n"\
            "Химия (16:00 - 17:00)\n"\
            "\n"\
            "Ядовитая инфа"
    with freeze_time("1 Jan 1970, 23:30"):
        assert command_next([]) == \
            "<b>Сегодня больше нет уроков</b>"
    with freeze_time("7 Aug 2005, 13:00"):
        assert command_next([]) == \
            "<b>Сегодня больше нет уроков</b>"
    with freeze_time("4 Nov 2021, 18:59"):
        assert command_next([]) == \
            "<b>Сегодня больше нет уроков</b>"


def test_now():
    """Test /now command."""
    with freeze_time("7 Jun 2022, 10:24"):
        assert command_now([]) == \
            "<b>Сейчас идёт:</b>\n"\
            "\n"\
            "Литература (10:00 - 11:00)\n"\
            "\n"\
            "Литературная инфа"

    with freeze_time("7 Jun 2022, 11:07"):
        assert command_now([]) == \
            "<b>Следующий урок:</b>\n"\
            "\n"\
            "Физика (12:00 - 13:00)\n"\
            "\n"\
            "Физическая инфа"

    with freeze_time("7 Jun 2022, 22:10"):
        assert command_now([]) == \
            "<b>Сегодня больше нет уроков</b>"

    with freeze_time("7 Aug 2005, 08:00"):
        assert command_now([]) == \
            "<b>Сегодня больше нет уроков</b>"


def test_schedule():
    """Test /schedule command."""
    with freeze_time("17 May 2022, 03:00"):
        assert command_schedule([]) == (
            "1. Литература (08:00 - 09:00)\n"
            "2. Литература (10:00 - 11:00)\n"
            "3. Физика (12:00 - 13:00)\n"
            "4. Физика (14:00 - 15:00)\n"
            "5. Физический практикум (16:00 - 17:00)\n"
            "6. Физический практикум (18:00 - 19:00)"
        )

    assert command_schedule(['сР']) == get_schedule_representation(2)
