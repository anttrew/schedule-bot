"""Test loading secrets from evironment."""

from helpers.env import BOT_TOKEN, HOMECHAT_ID


def test_bot_token():
    """Test `helpers.env.BOT_TOKEN` variable."""
    assert BOT_TOKEN == 'some_secret_token'


def test_home_chat():
    """Test `helpers.env.HOMECHAT_ID` variable."""
    assert HOMECHAT_ID == 100500
